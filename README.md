# vuetify-edge

## Problem
Vuetify v2.0.x throws an error when loading pages through Microsoft Edge on Windows 12; the console displays the following: 
`SCRIPT1028: Expected identifier, string or number`

## Package versions

- @vue/cli 3.11.0
- vuetify 2.0.17

## Problem Recreation
The master branch of this project is set up in a failed state; run the commands below then navigate to `http://localhost:8080` in Edge to duplicate the error:  
```
npm install
npm run serve
```

## Current Solution
Create a vue.config.json file if one does not already exist and add the following:
```
module.exports = {
  transpileDependencies: ['vuetify']
};
```

The `solution` branch contains these modifications; switch branches, rebuild the project, then reload in Edge